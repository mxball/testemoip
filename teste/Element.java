package teste;

public class Element{
	private String source;
	private int counter = 0;
	
	public Element(String source) {
		this.source = source;
	}
	
	public void add(){
		counter++;
	}
	
	public String getSource() {
		return source;
	}
	
	public int getCounter() {
		return counter;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Element other = (Element) obj;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return source + " - "+ counter;
	}

	
}