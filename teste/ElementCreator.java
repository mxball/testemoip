package teste;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import teste.Element.*;

public class ElementCreator {
	private ArrayList<Element> listaUrl = new ArrayList<>();
	private ArrayList<Element> listaStatus = new ArrayList<>();

	public ElementCreator(String fileName, String ulrRegex, String statusRegex){
		try {
			Scanner scanner = new Scanner(new File(fileName));
			Pattern patternRequest = Pattern.compile(ulrRegex);
			Pattern patternStatus = Pattern.compile(statusRegex);
			while(scanner.hasNextLine()){
				String line = scanner.nextLine();
				Matcher matcherRequest = patternRequest.matcher(line);
				Matcher matcherStatus = patternStatus.matcher(line);
				if(matcherRequest.find()){
					Element element = new Element(matcherRequest.group());
					if(listaUrl.contains(element)){
						int index = listaUrl.indexOf(element);
						listaUrl.get(index).add();;
					}else{
						listaUrl.add(element);
					}
				}
				if(matcherStatus.find()){
					Element element = new Element(matcherStatus.group());
					if(listaStatus.contains(element)){
						int index = listaStatus.indexOf(element);
						listaStatus.get(index).add();;
					}else{
						listaStatus.add(element);
					}
				}
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void printUrl(){
		listaUrl.sort((o1, o2) -> o2.getCounter() - o1.getCounter());
		for (Element element : listaUrl) {
			System.out.println(element.getSource() + " - " + element.getCounter());
		}
	}
	
	public void printStatus(){
		listaStatus.sort((o1, o2) -> o2.getCounter() - o1.getCounter());
		for (Element element : listaStatus) {
			System.out.println(element.getSource() + " - " + element.getCounter());
		}
	}
}
